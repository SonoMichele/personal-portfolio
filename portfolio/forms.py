# This file is a part of Michele Viotto's portfolio
# Copyright (C) 2020  Michele Viotto

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, SubmitField, TextAreaField, PasswordField
from wtforms.validators import DataRequired, Email, EqualTo


class ContactForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    message = TextAreaField('Message', validators=[DataRequired()])

    submit = SubmitField('Contact Me')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    
    submit = SubmitField('Log in')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])

    submit = SubmitField('Sign Up')


class AddProjectForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    role = StringField('Role', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    source_code_link =StringField('Source Code Link', validators=[DataRequired()])
    logo = FileField('Logo', validators=[DataRequired(), FileAllowed(['jpg', 'png'])])

    submit = SubmitField('Add Project')


class EditProjectForm(FlaskForm):
    title = StringField('Title')
    role = StringField('Role')
    description = TextAreaField('Description')
    source_code_link =StringField('Source Code Link')
    logo = FileField('Logo', validators=[FileAllowed(['jpg', 'png'])])

    submit = SubmitField('Edit Project')
