# This file is a part of Michele Viotto's portfolio
# Copyright (C) 2020  Michele Viotto

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, redirect, url_for, flash
from flask_login import login_required
from pony.orm import select

from portfolio.models import Project
from portfolio.utils import save_image, delete_image
from portfolio.forms import AddProjectForm, EditProjectForm


manage = Blueprint('manage', __name__)

@manage.route('/')
@login_required
def index():
    return render_template('manage.html')


@manage.route('/addproject', methods=['GET', 'POST'])
@login_required
def add_project():
    form = AddProjectForm()

    if form.validate_on_submit():
        image = save_image(form.logo.data)

        Project(
            title=form.title.data,
            role=form.role.data,
            description=form.description.data,
            source_code_link=form.source_code_link.data,
            logo=image
        )
        flash(f'Project {form.title.data} has been added!', 'success-message')
        return redirect(url_for('manage.index'))

    return render_template('add_project.html', form=form)


@manage.route('/removeproject')
@login_required
def remove_project():
    projects = select(p for p in Project)

    return render_template('removeproject.html', projects=projects)


@manage.route('/removeproject/<int:id>')
@login_required
def delete_project(id):
    Project[id].delete()
    flash(f'Project {id} has been deleted!', 'success-message')
    return redirect(url_for('manage.index'))


@manage.route('/editproject')
@login_required
def edit_project():
    projects = select(p for p in Project)
    
    return render_template('editproject.html', projects=projects)


@manage.route('editproject/<int:id>', methods=['GET', 'POST'])
@login_required
def actually_edit_project(id):
    project = Project[id]
    form = EditProjectForm()

    if form.validate_on_submit():
        project.title = form.title.data
        project.role = form.role.data
        print(form.description.data)
        project.description = form.description.data
        project.source_code_link = form.source_code_link.data
        if form.logo.data:
            logo = save_image(form.logo.data)
            delete_image(project.logo)
            project.logo = logo

        flash(f'Project {form.title.data} has been edited!', 'success-message')
        return redirect(url_for('manage.index'))

    return render_template('actuallyeditproject.html', form=form, project=project)
