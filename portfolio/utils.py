# This file is a part of Michele Viotto's portfolio
# Copyright (C) 2020  Michele Viotto

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import secrets
from PIL import Image

from flask import current_app


def save_image(image):
    name = secrets.token_hex(8)
    _, f_ext = os.path.splitext(image.filename)
    f_name = name + f_ext
    f_path = os.path.join(current_app.root_path, 'static/projects_logos', f_name)

    i = Image.open(image)
    i.save(f_path)

    return f_name


def delete_image(image):
    os.remove(os.path.join(current_app.root_path, 'static/projects_logos', image))

    return