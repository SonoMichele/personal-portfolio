# This file is a part of Michele Viotto's portfolio
# Copyright (C) 2020  Michele Viotto

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from flask import Flask, render_template, flash, redirect, url_for
from flask_login import LoginManager, login_required, login_user, logout_user
from flask_bcrypt import Bcrypt
from pony.flask import Pony
from pony.orm import Database, select

from portfolio.config import Config
from portfolio.forms import ContactForm, LoginForm, RegistrationForm


db = Database()
login_manager = LoginManager()
login_manager.login_view = 'login'
bcrypt = Bcrypt()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)


    db.bind(provider='sqlite', filename='portfolio.sqlite', create_db=True)
    from portfolio.models import User, Project
    db.generate_mapping(create_tables=True)


    @app.route('/', methods=['GET', 'POST'])
    def index():
        form = ContactForm()
        projects = select(p for p in Project)

        if form.validate_on_submit():
            from portfolio.bot import bot
            bot.chat(Config.TELEGRAM_OWNER_ID).send(
                f"*Nuovo messaggio dal sito*\n"
                f"*Da*: {form.name.data} ({form.email.data})\n"
                f"*Testo*: {form.message.data}"
            )
            flash('Messaggio inviato.', 'success-message')
            return redirect(url_for('index'))

        return render_template('index.html', form=form, projects=projects)
    

    @app.route('/login', methods=['GET', 'POST'])
    def login():
        form = LoginForm()

        if form.validate_on_submit():
            user = User.get(username=form.username.data)
            print(user)

            if user and bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('manage.index'))


        return render_template('login.html', form=form)
    

    @app.route('/register', methods=['GET', 'POST'])
    def register():
        form = RegistrationForm()

        if form.validate_on_submit():
            User(
                username=form.username.data,
                email=form.email.data,
                password=bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            )
            return redirect(url_for('login'))

        # return render_template('register.html', form=form)
        return redirect(url_for('index'))


    @app.route('/logout')
    @login_required
    def logout():
        logout_user()
        return redirect(url_for('index'))


    from portfolio.manage import manage
    app.register_blueprint(manage, url_prefix='/manage')


    Pony(app)
    login_manager.init_app(app)
    bcrypt.init_app(app)    

    return app