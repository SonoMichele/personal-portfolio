// This file is a part of Michele Viotto's portfolio
// Copyright (C) 2020  Michele Viotto

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const navbar = document.querySelector("#navbar")
const navContainer = document.querySelector("#navContainer")
const navBtn = document.querySelector("#navBtn")

navBtn.addEventListener('click', () => {
    navbar.classList.toggle('hidden')
})

document.body.onscroll = function() {
    if (window.pageYOffset > 0) {
        navContainer.classList.add("shadow-lg")
    } else {
        navContainer.classList.remove("shadow-lg")
    }
}


function showModal(modal_id) {
    var modal = document.querySelector("#" + modal_id)
    modal.classList.remove('hidden')
    modal.classList.add('block')
}

function hideModal(modal_id) {
    var modal = document.querySelector("#" + modal_id)
    modal.classList.remove('block')
    modal.classList.add('hidden')
}
