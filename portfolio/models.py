# This file is a part of Michele Viotto's portfolio
# Copyright (C) 2020  Michele Viotto

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from datetime import datetime

from pony.orm import PrimaryKey, Required
from flask_login import UserMixin

from portfolio import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.get(id=user_id)


class User(db.Entity, UserMixin):
    id = PrimaryKey(int, auto=True)
    username = Required(str, unique=True)
    email = Required(str, unique=True)
    password = Required(str, 60)


class Project(db.Entity):
    id = PrimaryKey(int, auto=True)
    title = Required(str)
    role = Required(str)
    description = Required(str)
    source_code_link = Required(str)
    logo = Required(str, 20)
    added_on = Required(datetime, default=lambda: datetime.now())
