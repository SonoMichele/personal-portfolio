FROM python:3.8 as base
RUN mkdir /code
WORKDIR /code

FROM base as builder
COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install --prefix=/install -r ./requirements.txt

FROM base
EXPOSE 8000
COPY --from=builder /install /usr/local
COPY ./portfolio /code/portfolio
CMD ["gunicorn", "-b 0.0.0.0:8000", "portfolio:create_app()"]