# portfolio

This is my personal portfolio webapp.

## Usage

### Using pip (venv)

```bash
$ pip install -r "requirements.txt"
$ gunicorn -b 0.0.0.0:8000 "portfolio:create_app()"
```
You can use the host and port that you want

### Using Docker
```bash
$ docker-compose up -d
```
You will also need a proxy to point to "portfolio:8000", you can use [Nginx Proxy Manager](https://nginxproxymanager.com/) and set it up like that:
```yaml
version: "3"
services:
  app:
    image: jc21/nginx-proxy-manager:2
    restart: always
    ports:
      # Public HTTP Port:
      - '80:80'
      # Public HTTPS Port:
      - '443:443'
      # Admin Web Port:
      - '81:81'
    #environment:
      # Uncomment this if IPv6 is not enabled on your host
      # DISABLE_IPV6: 'true'
    volumes:
      # Make sure this config.json file exists as per instructions above:
      - ./config.json:/app/config/production.json
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
    depends_on:
      - db
    networks:
      - backend
      - nginx-network
  db:
    image: jc21/mariadb-aria:10.4
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: 'npm'
      MYSQL_DATABASE: 'npm'
      MYSQL_USER: 'npm'
      MYSQL_PASSWORD: 'npm.'
    volumes:
      - ./data/mysql:/var/lib/mysql
    networks:
      - backend
networks:
  nginx-network:
    external:
      name: nginx-network
  backend:
```
Thanks to [matteob99](https://github.com/matteob99) for helping me with the docker setup.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPLv3](https://choosealicense.com/licenses/gpl-3.0/)